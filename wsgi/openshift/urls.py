from django.conf.urls import *
from django.contrib import admin
from django.views.generic import TemplateView

from parksearch import views as parksearch_views

from rest_framework import routers

# django admin
admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'searchers', parksearch_views.SearcherApiView)
router.register(r'leavers', parksearch_views.LeaverApiView)

urlpatterns = patterns('',
    ('', include(router.urls)),
    ('^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)
