from django.db import models

from rest_framework import serializers


class Searcher(models.Model):
    gcm_id = models.TextField(unique=True)
    lng = models.FloatField()
    lat = models.FloatField()
    r = models.FloatField(help_text="Radius in meters")

    class Meta:
        unique_together = ('lat', 'lng')


class SearcherSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'gcm_id', 'lng', 'lat', 'r')
        write_only_fields = ('gcm_id',)
        model = Searcher


class Leaver(models.Model):
    gcm_id = models.TextField(unique=True)
    lng = models.FloatField()
    lat = models.FloatField()
    timer = models.IntegerField(help_text="Time in minutes")
    timestamp = models.TimeField(auto_now=True)

    class Meta:
        unique_together = ('lat', 'lng')


class LeaverSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'gcm_id', 'lng', 'lat', 'timer', 'timestamp')
        write_only_fields = ('gcm_id',)
        model = Leaver
