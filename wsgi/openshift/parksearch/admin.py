from django.contrib import admin
import models


class SearcherAdmin(admin.ModelAdmin):
    list_display = ('id', 'gcm_id',)


class LeaverAdmin(admin.ModelAdmin):
    list_display = ('id', 'gcm_id', 'timer')

admin.site.register(models.Searcher, SearcherAdmin)
admin.site.register(models.Leaver, LeaverAdmin)
