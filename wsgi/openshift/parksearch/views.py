from rest_framework import viewsets, status
from rest_framework.response import Response
from models import Searcher, SearcherSerializer
from models import Leaver, LeaverSerializer

from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError
from django.conf import settings
from google_distance_matrix.core import DM

from datetime import datetime
import json
import requests


def send_gcm_message(regs_id, data, collapse_key=None):
    values = {
        'registration_ids': regs_id,
        'collapse_key': collapse_key,
        'data': data
    }

    values = json.dumps(values)

    headers = {
        'UserAgent': "GCM-Server",
        'Content-Type': 'application/json',
        'Authorization': 'key=' + settings.GCM_API_KEY,
    }

    response = requests.post(url="https://android.googleapis.com/gcm/send",
                             data=values,
                             headers=headers)

    print json.loads(response.content)

    return json.loads(response.content)


def filter_by_distance(origin, destinations, distance):
    if destinations:
        if type(destinations[0]) is Searcher:
            SerializerClass = SearcherSerializer
        elif type(destinations[0]) is Leaver:
            SerializerClass = LeaverSerializer

        tmp_destinations = [str(x).strip('()') for x in destinations.values_list('lat', 'lng')]
        print "DESTINATIONS: ", destinations.values_list('id')
        print "ORIGIN: ", origin
        d = DM()
        d.make_request(str(origin), tmp_destinations, mode='walking')
        ret = {}
        for k, v in d.get_closest_points(max_distance=float(distance)).iteritems():
            qs_element = destinations.get(lat=k.split(',')[0].strip(), lng=k.split(',')[1].strip())
            serializer = SerializerClass(qs_element)
            uid = serializer.data["id"]
            del[serializer.data["id"]]
            serializer.data['distance'] = v
            ret[uid] = serializer.data
        print "RET: ", ret

        return ret
    else:
        return {}


class SearcherApiView(viewsets.ModelViewSet):
    queryset = Searcher.objects.all()
    serializer_class = SearcherSerializer

    def create(self, request, pk=None):
        try:
            gcm_id = request.DATA["gcm_id"]
            lat = request.DATA["lat"]
            lng = request.DATA["lng"]
            r = request.DATA["r"]
            Leaver.objects.filter(gcm_id=gcm_id).delete()  # Always remove if it is parked
            searcher = Searcher.objects.filter(gcm_id=gcm_id)
            ret = super(SearcherApiView, self).create(request, pk=None)
            if ret.status_code == 201:
                filtereds = filter_by_distance(str(lat) + ',' + str(lng), Leaver.objects.exclude(Q(timer=None) | Q(timer=0)), r)
                return Response({'server_time': datetime.now().strftime("%H:%M"),
                                 'searcher': ret.data,
                                 'leavers': filtereds,
                                 'searchers': filter_by_distance(str(lat) + ',' + str(lng), Searcher.objects.exclude(id=ret.data['id']), r)}, status=ret.status_code)
            elif searcher:
                filtereds = filter_by_distance(str(lat) + ',' + str(lng), Leaver.objects.exclude(Q(timer=None) | Q(timer=0)), r)
                return Response({'server_time': datetime.now().strftime("%H:%M"),
                                 'searcher': SearcherSerializer(searcher[0]).data,
                                 'leavers': filtereds,
                                 'searchers': filter_by_distance(str(lat) + ',' + str(lng), Searcher.objects.exclude(id=searcher[0].id), r)}, status=status.HTTP_201_CREATED)
            else:
                return ret

        except (MultiValueDictKeyError, KeyError), e:
            print "EXCEPT: ", e
            return Response({'detail': "Invalid arguments",
                             'args': '["gcm_id", "lat", "lng", "r"]'})

    def update(self, request, pk=None):
        if "action" in request.DATA and request.DATA['action'] == "DELETE" and "gcm_id" in request.DATA:  # Patch for destroy, httpurlconnection does not support DELETE with body
                gcm_id = request.DATA["gcm_id"]
                obj = self.get_object()
                if obj.gcm_id == gcm_id:
                    return super(SearcherApiView, self).destroy(request)
                else:
                    return Response({'detail': "No permissions to edit this entry."}, status=status.HTTP_401_UNAUTHORIZED)

        else:
            try:
                gcm_id = request.DATA["gcm_id"]
                lat = request.DATA["lat"]
                lng = request.DATA["lng"]
                obj = self.get_object()
                r = request.DATA["r"]
                if obj.gcm_id == gcm_id:
                    ret = super(SearcherApiView, self).update(request)
                    if ret.status_code == 200:
                        return Response({'searchers': filter_by_distance(str(lat) + ',' + str(lng), Searcher.objects.exclude(id=ret.data['id']), r)}, status=ret.status_code)
                    else:
                        return ret
                else:
                    return Response({'detail': "No permissions to edit this entry."}, status=status.HTTP_401_UNAUTHORIZED)

            except (MultiValueDictKeyError, KeyError):
                return Response({'detail': "Invalid arguments",
                                 'args': '["gcm_id", "lat", "lng", "r"]'})

    def destroy(self, request, pk=None):
        # Called when mobile parks
        try:
            gcm_id = request.DATA["gcm_id"]
            obj = self.get_object()
            if obj.gcm_id == gcm_id:
                ret = super(SearcherApiView, self).destroy(request)
            else:
                return Response({'detail': "No permissions to edit this entry"}, status=status.HTTP_401_UNAUTHORIZED)

        except (MultiValueDictKeyError, KeyError):
            return Response({'detail': "Invalid arguments",
                             'args': '["gcm_id"]'})

        return ret


class LeaverApiView(viewsets.ModelViewSet):
    queryset = Leaver.objects.all()
    serializer_class = LeaverSerializer

    def create(self, request, pk=None):
        try:
            gcm_id = request.DATA["gcm_id"]
            ret = super(LeaverApiView, self).create(request)
            if ret.status_code == 201:
                interested_users = [x.encode("utf8") for x in Searcher.objects.values_list('gcm_id', flat=True)]  # TODO Filter by distance. Need to change filter_by_distance so you can ask which param you want to obtain (lat, ln, gcm_id, etc.)
                if interested_users:
                    send_gcm_message(interested_users, {'action': 'park', 'id': ret.data['id'], 'lat': request.DATA['lat'], 'lng': request.DATA['lng'], 'timer': request.DATA['timer']})

        except (MultiValueDictKeyError, KeyError):
            return Response({'detail': "Invalid arguments",
                             'args': '["gcm_id"]'})
        return ret

    def update(self, request, pk=None):
        if "action" in request.DATA and request.DATA['action'] == "DELETE" and "gcm_id" in request.DATA:  # Patch for destroy, httpurlconnection does not support DELETE with body
                gcm_id = request.DATA["gcm_id"]
                obj = self.get_object()
                if obj.gcm_id == gcm_id:
                    ret = super(LeaverApiView, self).destroy(request)
                    if ret.status_code == 204:
                        interested_users = [x.encode("utf8") for x in Searcher.objects.values_list('gcm_id', flat=True)]  # TODO Filter by distance. Need to change filter_by_distance so you can ask which param you want to obtain (lat, ln, gcm_id, etc.)
                        if interested_users:
                            send_gcm_message(interested_users, {'action': 'leave', 'id': obj.id})
                    return ret
                else:
                    return Response({'detail': "No permissions to edit this entry."}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            if "lat" in request.DATA or "lng" in request.DATA:
                return Response({'detail': "You can't update the coordinates"})
            gcm_id = request.DATA["gcm_id"]
            obj = self.get_object()
            if obj.gcm_id == gcm_id:
                # Send notification #TODO Notification #TODO Filter by distance
                return super(LeaverApiView, self).update(request)
            else:
                return Response({'detail': "No permissions to edit this entry"}, status=status.HTTP_401_UNAUTHORIZED)

        except (MultiValueDictKeyError, KeyError):
            return Response({'detail': "No permissions to edit this entry"})

    def destroy(self, request, pk=None):
        # Called when the timer expires in the mobile client
        try:
            gcm_id = request.DATA["gcm_id"]
            obj = self.get_object()
            if obj.gcm_id == gcm_id:
                ret = super(LeaverApiView, self).destroy(request)
                return ret
            else:
                return Response({'detail': "No permissions to edit this entry"}, status=status.HTTP_401_UNAUTHORIZED)

        except (MultiValueDictKeyError, KeyError):
            return Response({'detail': "No permissions to edit this entry"})
