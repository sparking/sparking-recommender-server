from setuptools import setup

import os

packages = ['Django==1.6.5', 'djangorestframework==2.3.13', 'simplejson', 'python-social-auth', 'requests==2.2.1', 'djangorestframework', 'django-filter', 'google-distance-matrix>=0.1.6']

setup(name='Sparking Recommender',
      version='1.0',
      description='Push BBVA',
      author='blck',
      author_email='manu.mirandad@gmail.com',
      url='sparking.cat',
      install_requires=packages,)
